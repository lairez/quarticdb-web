"use strict";

var d3 = require('d3');
var crossfilter = require('crossfilter2');
var dc = require('dc');

dc.config.defaultColors(d3.schemeCategory10);

var picrkChart = dc.barChart('#picrk-chart');
var discChart = dc.bubbleChart('#disc-chart');
var fullTable = dc.dataTable('.dc-data-table');
var count = dc.dataCount('.dc-data-count');
var nmonomialsChart = dc.barChart('#nmonomials-chart');
var endoringdimChart = dc.barChart('#endoringdim-chart');
var oddpicChart = dc.pieChart('#oddpic-chart');

global.picrkChart = picrkChart;
global.discChart = discChart;
global.nmonomialsChart = nmonomialsChart;
global.endoringdimChart = endoringdimChart;
global.dc = dc;

var exponent_regexp = /\^(\d+)/g ;
function display_polynomial(pol) {
    return pol.replace(exponent_regexp, '<sup>$1</sup>').replace(/\*/g, '');
}


var monomial_regexp = /([w-z](\^\d+)?\*?)+([-+ ]|$)/g;
function number_of_monomials(pol) {
    var m = pol.match(monomial_regexp);
    if (m == null) return 0;
    else return m.length;
}


d3.csv("quarticdb.csv").then(function(quartics) {

    // A little coercion, since the CSV is untyped.
    quartics.forEach(function(d, i) {
        d.index = i;
        d.picrk = +d.picrk;
        d.disc = +d.disc;
        d.nmonomials = number_of_monomials(d.pol);
        d.pol = display_polynomial(d.pol);
        d.endoringdim = +d.endoringdim;
        //d.totreal = (d.totreal == "True");
        d.logB = +d.logB;
        d.rarity = +d.rarity;
    });

    // Create the crossfilter for the relevant dimensions and groups.
    var quartic = crossfilter(quartics),
        all = quartic.groupAll(),
        id = quartic.dimension(function(d) { return d.index; }),
        picrk = quartic.dimension(function(d) { return d.picrk; }),
        picrks = picrk.group(), //.reduceCount(),
        disc = quartic.dimension(function(d) { return d.disc*32 + d.picrk; }),
        discs = disc.group().reduceCount(),
        nmon = quartic.dimension(function(d) { return d.nmonomials; }),
        nmons = nmon.group(),
        endoringdim = quartic.dimension(function(d) { return d.endoringdim; }),
        endoringdims = endoringdim.group(),
        oddpic = quartic.dimension(function(d) { return (d.picrk % 2 == 0) ? "even":"odd"; }),
        oddpics = oddpic.group()
        //totreal = quartic.dimension(function(d) { return d.totreal; }),
        //totreals = totreal.group()
    ;


    picrkChart
        .width(700)
        .height(400)
        .margins({top: 10, right: 50, bottom: 30, left: 60})
        .dimension(picrk)
        .group(picrks)
        .centerBar(true)
        .x(d3.scaleLinear().domain([0,21]))
        .y(d3.scalePow().domain([0,80000]).exponent(.2))
        .renderHorizontalGridLines(true)
    ;

    var yticks = [1,10,50,100,200,500,1000,2000,5000,10000,20000,30000,40000,60000,80000];
    function kiloticksfmt(v) {
        if (v < 1000) return v;
        else return Math.floor(v/1000) + 'k'; }

    picrkChart.yAxis().tickFormat(kiloticksfmt).tickValues(yticks);
    picrkChart.xAxis().tickValues([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]);

    discChart
        .width(700)
        .height(400)
        .margins({top: 10, right: 50, bottom: 30, left: 60})
        .dimension(disc)
        .group(discs)
        .renderLabel(false)
        .keyAccessor(d => (32 + (d.key % 32)) % 32)
        .valueAccessor(d => Math.abs(Math.floor(d.key / 32)))
        .radiusValueAccessor(d => 1+Math.log(d.value))
        .colorAccessor(d => 0)
        .minRadius(1)
        .x(d3.scaleLinear().domain([0,21]))
        .y(d3.scaleLog().domain([1,100000]).base(2))
        .renderHorizontalGridLines(true)
    ;
    discChart.xAxis().tickValues([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]);


    nmonomialsChart
        .width(300)
        .height(400)
        .margins({top: 10, right: 50, bottom: 30, left: 60})
        .dimension(nmon)
        .group(nmons)
        .centerBar(true)
        .x(d3.scaleLinear().domain([1,16]))
        .y(d3.scalePow().domain([0,80000]).exponent(.2))
        .renderHorizontalGridLines(true)
    ;

    nmonomialsChart.yAxis().tickFormat(kiloticksfmt).tickValues(yticks);
    nmonomialsChart.xAxis().ticks(20);

    endoringdimChart
        .width(300)
        .height(400)
        .margins({top: 10, right: 50, bottom: 30, left: 60})
        .dimension(endoringdim)
        .group(endoringdims)
        .centerBar(true)
        .x(d3.scaleLinear().domain([0,13]))
        .y(d3.scalePow().domain([0,80000]).exponent(.2))
        .renderHorizontalGridLines(true)
    ;

    endoringdimChart.yAxis().tickFormat(kiloticksfmt).tickValues(yticks);

    oddpicChart
        .width(200)
        .height(200)
        .radius(70)
        .dimension(oddpic)
        .group(oddpics)
    ;

    // totrealChart
    //     .width(200)
    //     .height(200)
    //     .radius(70)
    //     .dimension(totreal)
    //     .group(totreals)
    // ;


    fullTable
        .dimension(id)
        .group(d => d)
        .size(100)
        .columns([{label: 'polynomial', format: function(d) {return d.pol;}},
                  {label: 'ρ', format: function(d) {return d.picrk;}},
                  'disc',
                  {label: '#endo', format: function(d) { return d.endoringdim; }},
                  {label:'B', format: function(d){ return '10<sup>'+d.logB+'</sup>';}},
                  {label:'err', format: function(d){ return '10<sup>-'+d.rarity+'</sup>';}},
                  ])
        .order(d3.descending)
        .showGroups(false)
    ;

    count /* dc.dataCount('.dc-data-count', 'chartGroup'); */
        .dimension(quartic)
        .group(quartic.groupAll())
    ;

    dc.renderAll();

});
